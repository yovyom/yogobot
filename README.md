### Yogesh Ketkar (9-January-2022) ###


## A simple echo bot written with Golang ##
- Works with Bot Framework Emulator
- Works with Azure Bot Service (tested only with MS-Teams and Webchat)
- Handles only "message" type Activity
- Validates JWT which is signed with public key cryptography
- Gets an access_token from https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token before sending reply back to chat channels


## Building and running ##
- clone the repo
- go build .
- run the resulting binary <code>./yogobot</code>
- by default, it runs on port 3978. Only command line argument it accepts is the port number if you want to change the default port.