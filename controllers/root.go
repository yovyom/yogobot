package controllers

import (
	"fmt"
	"net/http"
	"yogobot/yohttp"
)

type rootController struct{}

func logHTTPRequest(r *http.Request) {
	body, _ := yohttp.GetHTTPRequestBody(r)
	fmt.Printf("URL: %s, Method: %s, Body: %s\n", r.URL, r.Method, body)
}

func Register() {
	rc := &rootController{}
	bot := newBot()
	http.Handle("/", rc)
	http.Handle("/api/messages", bot)
}

func (rc *rootController) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logHTTPRequest(r)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Hello, GO!"))
}
