package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"yogobot/models"
	"yogobot/yohttp"

	"github.com/golang-jwt/jwt"
)

type bot struct{}

// validateHTTPRequest does following
// - checks if "Authorization" header is present
// - checks if "Authorization" header is of the form "Bearer <token>"
// - validates the token
func validateHTTPRequest(r *http.Request) (bool, error) {
	a := r.Header["Authorization"]
	if len(a) == 0 {
		return false, fmt.Errorf("authorization header is missing")
	}
	b := strings.Split(a[0], " ")
	if len(b) != 2 {
		return false, fmt.Errorf("invalid authorization header")
	}
	tokenString := b[1]

	_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		certBytes, err := yohttp.GetCertificate(token)
		if err != nil {
			return nil, err
		}
		verifyKey, _ := jwt.ParseRSAPublicKeyFromPEM(certBytes)
		return verifyKey, nil
	})

	if err != nil {
		fmt.Println("JWT verification error", err)
		return false, err
	}

	fmt.Println("JWT verification success")
	return true, nil
}

func echoMessage(activity models.Activity) {
	// This step is needless in the context of Emulator
	access_token := yohttp.AuthenticateAzureBot()
	if access_token == "" {
		fmt.Println("couldn't authenticate with Azure bot")
	} else {
		yohttp.RespondToChatChannel(access_token, activity)
	}
}

func (b *bot) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Validate JWT. We still proceed even if validation fails
	// It will certainly fail in the context of Emulator as you don't get Authorization
	// header with Emulator
	validateHTTPRequest(r)
	// Convert HTTP body into Activity object
	body, _ := yohttp.GetHTTPRequestBody(r)
	dec := json.NewDecoder(strings.NewReader(string(body)))
	var activity models.Activity = models.Activity{}
	err := dec.Decode(&activity)
	if err == nil {
		if activity.Type == "message" {
			go echoMessage(activity)
		}
	}

	// logHTTPRequest(r)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Hello /api/messages!"))
}

func newBot() *bot {
	return &bot{}
}
