package models

type Activity struct {
	Type       string `json:"type"`
	ID         string `json:"id"`
	Timestamp  string `json:"timestamp"`
	ServiceURL string `json:"serviceUrl"`
	ChannelID  string `json:"channelId"`

	Conversation Conversation `json:"conversation"`
	From         From         `json:"from"`
	Recipient    Recipient    `json:"recipient"`

	TextFormat string `json:"textFormat"`
	Locale     string `json:"locale"`
	Text       string `json:"text"`
}

type Conversation struct {
	ID string `json:"id"`
}

type From struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Role string `json:"role"`
}

type Recipient struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
