package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"yogobot/controllers"
)

const (
	defaultPort = 3978
)

func getPort() int {
	port := defaultPort
	if len(os.Args) > 1 {
		var err error
		port, err = strconv.Atoi(os.Args[1])
		if err != nil {
			port = defaultPort
		}
	}
	return port
}

func main() {
	port := getPort()
	controllers.Register()
	fmt.Printf("Listening on port %d ...\n", port)
	http.ListenAndServe(":"+strconv.Itoa(port), nil)
}
