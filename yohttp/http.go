package yohttp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"yogobot/models"

	"github.com/golang-jwt/jwt"
)

// jwtKey has relevant attributes of the public corresponding
// to "kid" attribute found JWT body
type jwtPublicKey struct {
	KTY string   `json:"kty"`
	USE string   `json:"use"`
	KID string   `json:"kid"`
	X5T string   `json:"x5t"`
	X5C []string `json:"x5c"`
}

// jwtKeys is just a list of jwtKey objects
type jwtPublicKeys struct {
	KEYS []jwtPublicKey `json:"keys"`
}

const (
	// issuer is key in JWT body which identifies the issuer
	issuer = "iss"
	// configuration_suffix is added to the URL returned in JWT as "iss" to
	// get to the jwks_uri which stands for JWT KeySet URI
	configuration_suffix = "/.well-known/openid-configuration"

	// Azure Bot app id and secret
	app_id       = "7a58cc5e-0f76-40f5-a8c2-c2275bc64a6a"
	app_password = "R_U7Q~3bd4~RE5XNTs47LEFioM00bZDocihZ3"
)

var (
	// certMap is a map of key identifier to certificate
	// which acts as a cache for the certificates
	// slight problem: doesn't take into account certificate expiry
	certMap = map[string]string{}
)

// AuthenticateAzureBot authenticates Azure Bot app with its id and secret
// obtaines access_token which can be used to send responses back to
// chat channels over the "serviceUrl"
func AuthenticateAzureBot() string {
	url := "https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token"
	data := "grant_type=client_credentials&client_id=" + app_id + "&client_secret=" +
		app_password + "&scope=https%3A%2F%2Fapi.botframework.com%2F.default"
	res, err := http.Post(url, "application/x-www-form-urlencoded", strings.NewReader(data))
	if err == nil {
		body, _ := ioutil.ReadAll(res.Body)
		var result map[string]interface{}
		json.Unmarshal(body, &result)
		return result["access_token"].(string)
	}
	return ""
}

func RespondToChatChannel(access_token string, activity models.Activity) {
	from := activity.From
	activity.From.ID = activity.Recipient.ID
	activity.From.Name = activity.Recipient.Name
	activity.From.Role = ""
	activity.Recipient.ID = from.ID
	activity.Recipient.Name = from.Name
	activity.Text = "Echo " + activity.Text

	body, _ := json.Marshal(activity)
	fmt.Println("body", string(body))
	fmt.Println("ServieURL", activity.ServiceURL)

	url := activity.ServiceURL + "/v3/conversations/" + activity.Conversation.ID + "/activities/" + activity.ID
	r, err := http.NewRequest(http.MethodPost, url, strings.NewReader(string(body)))
	if err != nil {
		fmt.Println(err)
	}
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", "Bearer "+access_token)
	client := &http.Client{}
	res, err := client.Do(r)

	fmt.Println("response", res)
	fmt.Println("error", err)
}

// GetHTTPRequestBody returns the request body as string
func GetHTTPRequestBody(r *http.Request) (string, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err == nil {
		return string(body), nil
	} else {
		return "", fmt.Errorf("could not read request body: %w", err)
	}
}

// GetHTTPGetResponse makes an HTTP GET request on the 'url' passed
// returns the response body as []byte and an error if any
func GetHTTPGetResponse(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("received non-200 response code")
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read response body: %w", err)
	}
	return body, nil
}

// AddCertificateHeaderFooter adds "-----BEGIN CERTIFICATE-----\n" before and
// "-----END CERTIFICATE-----\n" after base64 encoded certificate string
// returns the certificate as []byte
func addCertificateHeaderFooter(c string) []byte {
	return []byte("-----BEGIN CERTIFICATE-----\n" + c + "\n-----END CERTIFICATE-----")
}

// GetCertificate returns x.509 certificate as []byte
// Function
// - starts with a JWT token and extracts the "iss"
// - appends configuration_suffix to "iss" URL and makes an HTTP GET request
// - extracts the "jwks_uri" from the resulting HTTP GET response and makes
//   a new HTTP GET request
// - extracts the certificate matching the "kid" found in the JWT body
func GetCertificate(token *jwt.Token) ([]byte, error) {
	kid := fmt.Sprintf("%v", token.Header["kid"])
	if certMap[kid] != "" {
		fmt.Println("certificate found in cache")
		return addCertificateHeaderFooter(certMap[kid]), nil
	}
	url1 := fmt.Sprintf("%v", token.Claims.(jwt.MapClaims)[issuer]) + configuration_suffix
	body, err := GetHTTPGetResponse(url1)
	if err != nil {
		return nil, fmt.Errorf("could not read response body: %w", err)
	}

	var result map[string]interface{}
	json.Unmarshal(body, &result)
	url2 := fmt.Sprintf("%v", result["jwks_uri"])

	body, err = GetHTTPGetResponse(url2)
	if err != nil {
		return nil, fmt.Errorf("could not read response body: %w", err)
	}

	dec := json.NewDecoder(strings.NewReader(string(body)))
	var jwtKeys jwtPublicKeys = jwtPublicKeys{}
	err = dec.Decode(&jwtKeys)
	if err != nil {
		return nil, err
	}

	for _, jwtKey := range jwtKeys.KEYS {
		if jwtKey.KID == kid {
			certMap[jwtKey.KID] = jwtKey.X5C[0]
			return addCertificateHeaderFooter(jwtKey.X5C[0]), nil
		}
	}

	return nil, nil
}
